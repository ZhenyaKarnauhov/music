package ru.kev.musicdemo;


import java.io.*;
import java.net.URL;
import java.nio.channels.*;

/**
 * Класс, в котором реализовано скачивание музыки, по URL адресам
 * из текствого файла outFIle.txt.
 *
 * @author Evgeniy Karnauhov 15ИТ18
 */
public class DownloadMusic extends Thread {

    private String resultFile;

    private static final String PATH_TO_MUSIC = "src\\ru\\kev\\musicdemo";

    DownloadMusic(String resultFile) {
        this.resultFile = resultFile;
    }

    @Override
    public void run() {
        try (BufferedReader musicFile = new BufferedReader(new FileReader(resultFile))) {
            long beforeDownload = System.currentTimeMillis();
            String music;
            int count = 0;
            while ((music = musicFile.readLine()) != null) {
                System.out.println("Началась загрузка " + (count + 1));
                downloadUsingNIO(music, PATH_TO_MUSIC + String.valueOf(count + 1) + ".mp3");
                count++;
                long timeDownload = System.currentTimeMillis() - beforeDownload;
                System.out.println("Загрузка завершена. Время загрузки: " + (timeDownload / 1000) + " секунд");
                beforeDownload = System.currentTimeMillis();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод, который по ссылке скачивает музыку.
     *
     * @param linkMusic ссылка для скачивания
     * @param fileName имя файла
     * @throws IOException исключение
     */
    private static void downloadUsingNIO(String linkMusic, String fileName) throws IOException {
        URL url = new URL(linkMusic);
        try (ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
             FileOutputStream stream = new FileOutputStream(fileName)) {
            stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        }
    }
}