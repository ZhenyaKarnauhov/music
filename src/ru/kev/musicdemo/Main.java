package ru.kev.musicdemo;

import java.io.*;
import java.net.URL;
import java.util.regex.*;
import java.util.stream.Collectors;

/**
 * Класс, в котором реализовано выполнение задачи "Качаем музыку".
 *
 * @author Evgeniy Karnauhov 15ИТ18
 */
public class Main {

    private static final String LINK_SITE_TXT = "src\\ru\\kev\\musicdemo\\inFile.txt";
    private static final String OUTPUT_FILE_TXT = "src\\ru\\kev\\musicdemo\\outFile.txt";

    public static void main(String[] args) throws InterruptedException {

        DownlLinks();
        new DownloadMusic(OUTPUT_FILE_TXT).start();
    }

    /**
     * Метод который с помощью шаблона находит ссылки и записывает в текстовый файл.
     */
    private static void DownlLinks() {
        String Url;
        String result;
        try (BufferedReader inFile = new BufferedReader(new FileReader(LINK_SITE_TXT));
             BufferedWriter outFile = new BufferedWriter(new FileWriter(OUTPUT_FILE_TXT))) {
            while ((Url = inFile.readLine()) != null) {
                URL url = new URL(Url);
                try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
                    result = bufferedReader.lines().collect(Collectors.joining("\n"));
                }

                Pattern email_pattern = Pattern.compile("\\s*(?<=data-url\\s?=\\s?\")[^>]*\\/*(?=\")");
                Matcher matcher = email_pattern.matcher(result);

                int i = 0;
                while (matcher.find() && i < 4) {
                    outFile.write(matcher.group() + "\n");
                    i++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}